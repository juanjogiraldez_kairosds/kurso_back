'use strict'

const passport = require('passport');
const BasicStrategy = require('passport-http').BasicStrategy;
const verify = require('./middlewares/authVerify.js');
const JwtStrategy = require('passport-jwt').Strategy;;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const userRepository= require('./items/user/repository.js');
const SECRET_KEY = "SECRET_KEY";
const jwtOpts = {jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),secretOrKey: SECRET_KEY};

module.exports = app => { 
    app.use(passport.initialize());
    passport.use(new BasicStrategy(verify));
    passport.use(new JwtStrategy(jwtOpts, async (payload, done) => {
    
        const current =payload.body
        const user = await userRepository.getFind({user: current.userName});
   
        if (user) {     
                return done(null, user);     
            } else {
                return done(null, false, { message: 'User not found' });     
         
                }          
            }
        ) 
    );

}