'use strict'

const OffensiveWordService = require('./service.js')

const OffensiveWordcontroller = {}

OffensiveWordcontroller.getAll =  async (req, res, next) => {
    try {
        const result = await OffensiveWordService.getAll();
        res.status(200).json(result);
    } catch (error) {
        console.log(error)
    }
};

OffensiveWordcontroller.add = async(req, res, next) => {
    try {
        const offensiveword = req.body;
        const result = await OffensiveWordService.add(offensiveword);
        res.status(201).json(result);   
    } catch (error) {
        console.log(error)
    }
};

OffensiveWordcontroller.update = async(req, res, next) => {
    try {
        const {id} = req.params
        const offensiveword = req.body;
        const result = await OffensiveWordService.update(id, offensiveword);
        res.status(200).json(result);
    } catch (error) {
        console.log(error)
    }
};

OffensiveWordcontroller.delete = async(req, res, next) => {
    try {
        const id = req.params.id;
        const result = await OffensiveWordService.delete(id);
        res.status(200).json(result);
    } catch (error) {
        console.log(error)
    }
}

module.exports = OffensiveWordcontroller