'use strict'

const OffensiveWordSchema = require('./model.js');

const OffensiveWordRepository = {};

OffensiveWordRepository.addOffensiveWord = async (offensiveworld) => {
    const newOffensiveWorld = new OffensiveWordSchema(offensiveworld);
    return await newOffensiveWorld.save();
}

OffensiveWordRepository.getAll = async () => {
    return await OffensiveWordSchema.find({});
}

OffensiveWordRepository.delete = async (id) => {
    return await OffensiveWordSchema.findByIdAndDelete(id);
}

OffensiveWordRepository.update = async (id, offensiveword) => {
    return await OffensiveWordSchema.findByIdAndUpdate(id, offensiveword, { new: true });
}

module.exports = OffensiveWordRepository;