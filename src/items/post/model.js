'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = Schema({
    author: {type: "String"},
    nickName: {type: "String"},
    title: {type: "String"}, 
    text: {type: "String"},
    postIdAuthor: { type: "String" },
    comments: [{ type: Schema.Types.ObjectId,  ref: 'comments'}]

},{
    timestamps: true
});

module.exports = mongoose.model('posts', PostSchema);