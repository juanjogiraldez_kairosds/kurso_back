'use strict'

const UserRepository = require('../items/user/repository.js');

module.exports = async () => {
    try {
        const usersInitial = await UserRepository.getFind() ;
        if (usersInitial.length === 0) {
            UserRepository.addUser({user: 'userAlfa', passwordHash: 'alfa', role: 'Admin'})
            UserRepository.addUser({user: 'userOmega', passwordHash:'omega', role: 'Admin'})
            console.info(' Users basics ');
            }
        } catch (err) {
        console.log(err);
    }
}