'use strict'

const OffensiveWordController = require('../items/offensiveWord/controller.js');
const UserControl = require('../middlewares/userControl.js')
const express = require('express');
const passport = require('passport');
const offensiveWord = express.Router();

offensiveWord.get('/', OffensiveWordController.getAll);
offensiveWord.post('/', passport.authenticate('jwt', { session: false }), UserControl.realManager , OffensiveWordController.add);
offensiveWord.put('/:id', passport.authenticate('jwt', { session: false }), UserControl.realManager, OffensiveWordController.update);
offensiveWord.delete('/:id', passport.authenticate('jwt', { session: false }), UserControl.realManager, OffensiveWordController.delete);



module.exports = offensiveWord