'use strict'

const UserPost = require('../items/post/controller.js');
const OffensiveValidator = require('../middlewares/validator.js')
const passport = require('passport');
const express = require('express');
const post = express.Router();
const UserControl = require('../middlewares/userControl.js')


post.get('/', UserPost.getAll);
post.get('/:id', UserPost.getById);
post.post('/', passport.authenticate('jwt', { session: false }), UserControl.createEntrie , UserPost.addPost);
post.put('/:id',  passport.authenticate('jwt', { session: false }), UserControl.updateOnwPost,  UserPost.updatePost );
post.delete('/:id',  passport.authenticate('jwt', { session: false }),UserControl.deleteOwnPost,  UserPost.deletePost);
post.put('/:id/comment', passport.authenticate('jwt', { session: false }),  OffensiveValidator.checkwords , UserPost.addComment); // passport.authenticate('jwt', { session: false }), OffensiveValidator.checkwords , 

module.exports = post